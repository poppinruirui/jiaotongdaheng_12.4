﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CyberTreeMath : MonoBehaviour {

    public const float ANGLE_TO_RADIAN_XISHU = 0.017453f;

    public const int SECONDS_PER_MIN = 60;
    public const int SECONDS_PER_HOUR = 3600;
    public const int SECONDS_PER_DAY = 86400;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public static float Angle2Radian(float fAngle)
    {
        return ANGLE_TO_RADIAN_XISHU * fAngle;
    }

    public static float Cos(float fAngle)
    {
        return Mathf.Cos( Angle2Radian( fAngle ) );
    }

    public static float Sin(float fAngle)
    {
        return Mathf.Sin(Angle2Radian(fAngle));
    }

    // 已知位移和时间求初速度（末速度为零）
    public static float GetV0(float s, float t)
    {
        return (2.0f * s / t);
    }

    // 已知位移和时间求加速度（末速度为零）
    public static float GetA(float s, float t)
    {
        return -2.0f * s / (t * t);
    }

    public static string FormatMoney(double nValue )
    {
        /*
        if ( nValue < 1000 )
        {
            return nValue.ToString();
        }
        else if (nValue < 1000000)
        {
            float fK = (float)nValue / 1000;
            return fK.ToString( "f2" ) + "K";
        }
        else
        {
            float fM = (float)nValue / 1000000;
            return fM.ToString("f2") + "M";
        }
        */





        return nValue.ToString();
    }

    public static string GetFormatMoney(double nValue)
    {
        double dStep = 1000d;
        double dRet = nValue / dStep;
        double dLastRet = 0d;
        int nCount = 0;
        while( dRet >= 1 )
        {
            nCount++;
            dStep *= 1000d;
            dLastRet = dRet;
            dRet = nValue / dStep;
        }

        if (nCount < 2)
        {
            return nValue.ToString("f0");
        }

        dLastRet *= 1000d;
        string szLastRet = dLastRet.ToString("f2");
        if ( nCount == 2 ) 
        {
            szLastRet += "k";

        }
        else if (nCount == 3) 
        {
            szLastRet += "m";
        }
        else if  (nCount == 4)
        {
            szLastRet += "b";
        }
        else
        {
            szLastRet += "a";
            szLastRet += (char)( nCount + 92);
        }

        return szLastRet;
    }


    public static string FormatTime( int nSeconds )
    {
        string szResult = "";

        int nType = 0;

        int nDay = 0;
        int nHour = 0;
        int nMin = 0;
        if (nSeconds >= SECONDS_PER_DAY ) // 超过一天
        {
            nDay = nSeconds / SECONDS_PER_DAY;
            nSeconds = nSeconds % SECONDS_PER_DAY;
            nType = 3;
        }
        if (nSeconds >= SECONDS_PER_HOUR) // 超过1小时
        {
            nHour = nSeconds / SECONDS_PER_HOUR;
            nSeconds = nSeconds % SECONDS_PER_HOUR;
            if ( nType < 2 )
            {
                nType = 2;
            }
        }
        if (nSeconds >= SECONDS_PER_MIN ) // 超过1分钟 
        {
            nMin = nSeconds / SECONDS_PER_MIN;
            nSeconds = nSeconds % SECONDS_PER_MIN;
            if (nType < 1)
            {
                nType = 1;
            }
        }

        if (nType == 3)
        {
            szResult = nDay + "天" + nHour + "时" + nMin + "分" + nSeconds + "秒";
        }
        else if (nType == 2)
        {
            szResult =  nHour + "时" + nMin + "分" + nSeconds + "秒";
        }
        else if (nType == 1)
        {
            szResult =  nMin + "分" + nSeconds + "秒";
        }
        else
        {
            szResult =  nSeconds + "秒";
        }





        return szResult;
    }


    // include nStartIndex and EndIndex
    public static int GetRandomIndex( int nStartIndex, int EndIndex )
    {
        return UnityEngine.Random.Range(nStartIndex, EndIndex);
    }


} // end class
