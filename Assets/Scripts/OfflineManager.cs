﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OfflineManager : MonoBehaviour {

    public static OfflineManager s_Instance = null;

    /// <summary>
    /// UI
    /// </summary>
    public Text _txtWatchAdsPromote;
    public Text _txtDiamondPromote;

    /// end UI

    float m_fWatchAdsPromote_Initial = 0;
    float m_fDiamondPromote_Initial = 0;
    int m_nDiamondCost_Initial = 0;

    float m_fWatchAdsPromote_Real = 0;
    float m_fDiamondPromote_Real = 0;


    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {

        string szConfigFileName_Offline = DataManager.url + "offline.csv";
        StartCoroutine(LoadConfig_Offline(szConfigFileName_Offline));
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    IEnumerator LoadConfig_Offline(string szFileName)
    {
        WWW www = new WWW(szFileName);
        yield return www; // 等待下载

        string[] aryLines = www.text.Split('\n');
        string[] aryGeneralParams = aryLines[1].Split( ',' );
        int nIndex = 0;
        m_fWatchAdsPromote_Initial = float.Parse(aryGeneralParams[nIndex++]);
        m_fDiamondPromote_Initial = float.Parse(aryGeneralParams[nIndex++]);
        m_nDiamondCost_Initial = int.Parse(aryGeneralParams[nIndex++]);



    }


    public void UpdateOfflineProfitInfo()
    {
        float fTalentPromote = ScienceTree.s_Instance.GetOfflineProfitPromote(); 

        m_fWatchAdsPromote_Real = m_fWatchAdsPromote_Initial * ( 1f + fTalentPromote);
        m_fDiamondPromote_Real = m_fDiamondPromote_Initial * (1f + fTalentPromote);

        _txtWatchAdsPromote.text = "看广告" + " x" + (1f + m_fWatchAdsPromote_Real);
        _txtDiamondPromote.text = "用" + m_nDiamondCost_Initial + "钻石 x" + (1f + m_fDiamondPromote_Real);
    }

    public float GetRealWatchAdsPromotePercent()
    {
        return m_fWatchAdsPromote_Real;
    }

    public float GetRealUsingDiamondPromotePercent()
    {
        return m_fDiamondPromote_Real;
    }

    public int GetRealDiamondCost()
    {
        return m_nDiamondCost_Initial;
    }

} // end class
