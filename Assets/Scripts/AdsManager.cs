﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AdsManager : MonoBehaviour {

    public static AdsManager s_Instance = null;

    public float m_fTotalTime = 10000;
    float m_fCurTime = 0;

    public GameObject _panelAds;

    /// <summary>
    /// UI
    /// </summary>
    public GameObject _panelPreAds;
    public Text _txtAdsLeftTime;

    public Text _txtTitle;
    public Text _txtDesc;
    public Text _txtLeftTime;

    public Image _imgProgressBar;

    public float m_fBaseRaise = 2f;     // 广告的金币加成百分比
    public float m_fRaiseDuration = 0f; // 广告加成效果的持续时间
    public float m_fAdsPlayTime = 0f; // 一则广告的平均播放时长

    // 广告的实际加成、实际持续时间、实际总时间还要受天赋点的影响，不一定等于初始配置的数值。
    float m_fRealCoinPromote = 0f;
    float m_fRealDuraton = 0f;
    float m_fRealTotalTime = 0f;

    eAdsType m_eAdsType = eAdsType.common;

    


    public enum eAdsType
    {
        common,
        bat_collect_offline,
        bat_ads_raise,
        collect_offline_profit_watching_ads,
        collect_offline_profit_using_diamond,
    };

    public void SetAdsType(eAdsType eType)
    {
        m_eAdsType = eType;
    }

    private void Awake()
    {
        s_Instance = this;
    }


    // Use this for initialization
    void Start () {
        string szConfigFileName_Ads = DataManager.url + "ads.csv";
        StartCoroutine(LoadConfig_Ads(szConfigFileName_Ads));
    }
	
	// Update is called once per frame
	void Update () {
        MainLoop();
	}


    IEnumerator LoadConfig_Ads(string szFileName)
    {
        WWW www = new WWW(szFileName);
        yield return www; // 等待下载

        string[] aryLines = www.text.Split('\n');

        string[] aryGeneralParams = aryLines[1].Split( ',' );
        int nIndex = 0;
        m_fAdsPlayTime = float.Parse(aryGeneralParams[nIndex++]);
        m_fRaiseDuration = float.Parse(aryGeneralParams[nIndex++]);
        m_fBaseRaise = float.Parse(aryGeneralParams[nIndex++]);
        m_fTotalTime = float.Parse(aryGeneralParams[nIndex++]);

    } // end LoadConfig_Ads




    public  void OnClick_CloseAds()
    {
        _panelAds.SetActive(false);

        switch (m_eAdsType)
        {
            case eAdsType.common:
                {
                    //MapManager.s_Instance.GetCurDistrict().GainAdsRaise();
                    Bat_Ads_Raise();
                }
                break;

            case eAdsType.bat_collect_offline:
                {
                    CollectAllDistrictsOfflineGainOfThisPlanet();
                }
                break;
            case eAdsType.bat_ads_raise:
                {
                    Bat_Ads_Raise();
                }
                break;
            case eAdsType.collect_offline_profit_watching_ads:
                {
                    District district = MapManager.s_Instance.GetCurDistrict();
                    float fOfflineProfit = Main.s_Instance.m_fOfflineProfit * ( 1f + OfflineManager.s_Instance.GetRealWatchAdsPromotePercent() );
                    Debug.Log( "获得离线收益：" + fOfflineProfit);


                    Planet planet = MapManager.s_Instance.GetCurPlanet();

                    planet.SetCoin(planet.GetCoin() + (int)fOfflineProfit);
                    district.SetCurTotalOfflineGain(0);

                    Main.s_Instance.CollectOfflineProfitAnimation();
                }
                break;

            case eAdsType.collect_offline_profit_using_diamond:
                {
                    District district = MapManager.s_Instance.GetCurDistrict();
                    float fOfflineProfit = Main.s_Instance.m_fOfflineProfit * (1f + OfflineManager.s_Instance.GetRealUsingDiamondPromotePercent());
                    Debug.Log("获得离线收益：" + fOfflineProfit);


                    Planet planet = MapManager.s_Instance.GetCurPlanet();

                    planet.SetCoin(planet.GetCoin() + (int)fOfflineProfit);
                    district.SetCurTotalOfflineGain(0);

                    Main.s_Instance.CollectOfflineProfitAnimation();
                }
                break;

        } // end switch
    }

    public float GetAdsDuration()
    {
        float fRealDuration = 0;

        return m_fRealDuraton;
    }

    public void Bat_Ads_Raise()
    {
        Planet planet = MapManager.s_Instance.GetPlanetById(MapManager.s_Instance.m_nCurShowPlanetDetialIdOnUI);
        District[] aryDistricts = planet.GetDistrictsList();
        for (int i = 0; i < aryDistricts.Length; i++)
        {
            District district = aryDistricts[i];
            if (district.GetStatus() != MapManager.eDistrictStatus.unlocked)
            {
                continue;
            }
            district.GainAdsRaise();
        }

        m_fCurTime += m_fAdsPlayTime;
        _imgProgressBar.fillAmount = m_fCurTime / m_fTotalTime;
    }

    // right here
    void CollectAllDistrictsOfflineGainOfThisPlanet()
    {
        string szDetails = "";
        float fTotal = 0;
        Planet planet = MapManager.s_Instance.GetPlanetById(MapManager.s_Instance.m_nCurShowPlanetDetialIdOnUI);
        District[] aryDistricts = planet.GetDistrictsList();
        for (int i = 0; i < aryDistricts.Length; i++ )
        {
            District district = aryDistricts[i];
            if (district.GetStatus() != MapManager.eDistrictStatus.unlocked)
            {
                szDetails += ("<Color=#C8C8C8>赛道未解锁</Color>\n");
                continue;
            }
            if (district == MapManager.s_Instance.GetCurDistrict())
            {
                szDetails += ("<Color=#C8C8C8>当前赛道</Color>\n");
                continue;
            }
            float fGainOfThisDistrict = district.CalculateOffLineProfit();
            fTotal += fGainOfThisDistrict;
            district.SetCurTotalOfflineGain(0);
            szDetails += ("<Color=#FEE834>" +  fGainOfThisDistrict.ToString("f0") + "</Color>" + "\n" );
        } // end for 

 
        float fDouble = fTotal * 2;
        MapManager.s_Instance._txtBatOfflineGain.text = fTotal.ToString( "f0" ) + " X 2 = " + fDouble.ToString( "f0" );
        MapManager.s_Instance._txtBatOfflineDetail.text = szDetails;

        MapManager.s_Instance._subpanelBatCollectOffline.SetActive(true );

        planet.SetCoin( planet.GetCoin() + (int)fTotal);
    }

    public void OnClick_ClosebatCollectionPanel()
    {
        MapManager.s_Instance._subpanelBatCollectOffline.SetActive(false);

    }

    public float GetAdsRaise( ref string szAdsPromoteInfo)   
    {
        // return m_fBaseRaise * ( 1f + ScienceTree.s_Instance.GetAdsCoinRaise() );
       
        szAdsPromoteInfo = "";
        szAdsPromoteInfo += "广告总加成: X" + m_fRealCoinPromote.ToString("f1") + "倍\n";
        szAdsPromoteInfo += "--------------------------------\n";

        return m_fRealCoinPromote;
    }

    public void OnClick_OpenAds()
    {
        _panelPreAds.SetActive( true );


        UpdateRealData();
    }

    void UpdateRealData()
    {
        float fPromotePercent = ScienceTree.s_Instance.GetAdsPromotePercent();
        m_fRealCoinPromote = (1f + m_fBaseRaise);
        if (fPromotePercent > 0)
        {
            m_fRealCoinPromote = (1f + fPromotePercent) * m_fRealCoinPromote;
        }

        float fDurationPromotePercent = ScienceTree.s_Instance.GetAdsDurationPromotePercent();
        m_fRealDuraton = m_fRaiseDuration;
        if (fDurationPromotePercent > 0)
        {
            m_fRealDuraton = (1f + fDurationPromotePercent) * m_fRealDuraton;
        }

        _txtTitle.text = "X" + m_fRealCoinPromote.ToString("f2") + "倍广告收入";
        _txtDesc.text = CyberTreeMath.FormatTime( (int)m_fRealDuraton ) + " 内 " + m_fRealCoinPromote.ToString("f2") + "倍广告收入";

        float fTotalTimePromotePercent = ScienceTree.s_Instance.GetAdsTotalTimePromotePercent();
        m_fRealTotalTime = m_fTotalTime;
        if (fTotalTimePromotePercent > 0)
        {
            m_fRealTotalTime = (1f + fTotalTimePromotePercent) * m_fRealTotalTime;
        }
        // m_fRealTotalTime
    }

    public void PlayAds()
    {
        Handheld.PlayFullScreenMovie("ads0.mp4", Color.black, FullScreenMovieControlMode.CancelOnInput);
    }

    public void OnClick_WatchAds()
    {
        if ( m_fTotalTime - m_fCurTime < m_fAdsPlayTime  )
        {
            UIMsgBox.s_Instance.ShowMsg( "广告剩余总时间不足" );
            return;
        }

        _panelAds.SetActive(true);
        SetAdsType(eAdsType.common);
        // _panelPreAds.SetActive(false);

        Handheld.PlayFullScreenMovie("ads0.mp4", Color.black, FullScreenMovieControlMode.CancelOnInput);
        OnClick_CloseAds();
    }

    public void OnClick_ClosePreAds()
    {
        _panelPreAds.SetActive(false);
    }

    public void OnClick_OpenAds_CollectAllDistrcitOfflineGainsOfThisPlanet()
    {
        _panelAds.SetActive(true);
        SetAdsType(eAdsType.bat_collect_offline);

        Handheld.PlayFullScreenMovie("ads0.mp4", Color.black, FullScreenMovieControlMode.CancelOnInput);
        OnClick_CloseAds();

    }

    public void OnClick_OpenAds_BatAdsRaise()
    {
        _panelAds.SetActive(true);
        SetAdsType(eAdsType.bat_ads_raise);

        Handheld.PlayFullScreenMovie("ads0.mp4", Color.black, FullScreenMovieControlMode.CancelOnInput);
        OnClick_CloseAds();
    }

    float m_fMainLoopTimeElapse = 0f;
    public void MainLoop()
    {
        m_fMainLoopTimeElapse += Time.deltaTime;
        if (m_fMainLoopTimeElapse < 1f)
        {
            return;
        }
        m_fMainLoopTimeElapse = 0;

        Planet[] aryPlanets = MapManager.s_Instance.GetPlanetList();
        if (aryPlanets == null)
        {
            return;
        }
        for (int i = 0; i < aryPlanets.Length; i++ )
        {
            Planet planet = aryPlanets[i];
            District[] aryDistricts = planet.GetDistrictsList();
            for (int j = 0; j < aryDistricts.Length; j++ )
            {
                District district = aryDistricts[j];
                district.AdsRaiseTimeLoop();
                if (district == MapManager.s_Instance.GetCurDistrict())
                {
                    ShowLeftTime(district.GetAdsLeftTime());
                }
            } // end for j
        } // end for i

        UpdateTime();

    }

    void UpdateTime()
    {
       


        m_fCurTime -= 1f;
        if (m_fCurTime <= 0)
        {
            m_fCurTime = 0;
        }
        _imgProgressBar.fillAmount = m_fCurTime / m_fRealTotalTime;
        _txtLeftTime.text = CyberTreeMath.FormatTime((int)m_fCurTime) + "/" + CyberTreeMath.FormatTime( (int)m_fRealTotalTime);
    }

    public void ShowLeftTime( float fLeftTime )
    {
        if (fLeftTime <= 0)
        {
            _txtAdsLeftTime.text = "";
        }
        else
        {
            _txtAdsLeftTime.text = "剩余时间：" + fLeftTime.ToString();
        }
    }


} // end class
