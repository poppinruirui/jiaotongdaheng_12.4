﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class District : MonoBehaviour {

    public Planet m_BoundPlanet = null;

    public MapManager.eDistrictStatus m_eStatus = MapManager.eDistrictStatus.unlocked;
    public int m_nId = 0;

    public int m_nUnlockPrice = 0; //  解锁该区域所需的金币数量


    string m_szData = "";
    int m_nPrestigeTimes = 0;

    int[] m_aryVehicleBuyTimes = new int[100];
    Dictionary<int, int> m_dicVehicleBuyTimes = new Dictionary<int, int>();

    int m_nAdsRaiseTime = 0;

    // 主动技能的信息(技能是绑定在每个赛道上的，每个赛道有自己的技能状态)
    Dictionary<SkillManager.eSkillType, Skill> m_dicSkill = new Dictionary<SkillManager.eSkillType, Skill>();

    SkillManager.sSkillConfig config;

    public int m_nAdsBaseTime = 60;

    List<Plane> m_lstRunningPlanes = new List<Plane>();

    int m_nLevel = 1; // 赛道等级：本赛道上已解锁的最高级交通工具的等级就是赛道等级

    /// <summary>
    /// 主管相关
    /// </summary>
    int m_nBuyAdminTimes = 0; // 主管招募次数
 
    UIAdministratorCounter m_CurUsingAdmin = null;
    List<Admin> m_lstAdmins = new List<Admin>();
    // end 主管相关

    /// <summary>
    /// 能源研究院相关
    /// </summary>
    ResearchCounter m_ResearchCounter = null;
    // end 能源研究院相关

    /// <summary>
    ///  掉落相关
    /// </summary>
    int m_nDropLevel = 0; // 可以掉落的载具等级
    int m_nDropInterval = 0; // 掉落的时间间隔
    /// end 掉落相关

    // Use this for initialization
    void Start () {
        for (int i = 0; i < 100; i++)
        {
            m_aryVehicleBuyTimes[i] = 0;
        }

        for (int i = 0; i < (int)SkillManager.eSkillType.total_num; i++ )
        {
            Skill skill = ResourceManager.s_Instance.NewSkill();
            skill.SetBound(m_BoundPlanet, this );
            SkillManager.eSkillType eType = (SkillManager.eSkillType)i;
            skill.SetType(eType);
            ScienceTree.s_Instance.GetSkillConfig(eType, ref config);
            skill.SetConfig(config);


            m_dicSkill[(SkillManager.eSkillType)i] = skill;


        }

   


    } // end start

    // 获取这个赛道所属的星球
    public Planet GetBoundPlanet()
    {
        return m_BoundPlanet;
    }

    public Skill GetSkill( SkillManager.eSkillType eType )
    {
        Skill skill = null;
        if ( !m_dicSkill.TryGetValue(eType, out skill) )
        {
            Debug.LogError( "bug!" );
        }
        return skill;
    }
	
	// Update is called once per frame
	void Update () {
        OfflineLoop();

       
    }

    public void SetStatus(MapManager.eDistrictStatus eStatus)
    {
        m_eStatus = eStatus;
    }

    public MapManager.eDistrictStatus GetStatus()
    {
        return m_eStatus;
    }


    public int GetId()
    {
        return m_nId;
    }

    public void SetData( string szData )
    {
        m_szData = szData;
    }

    public string GetData()
    {
        return m_szData;
    }

    public double GetUnlockPrice()
    {
        return DataManager.s_Instance.GetTrackConfigById(this.GetBoundPlanet().GetId(), this.GetId()).nUnlockPrice;

        // return m_nUnlockPrice;
    }

    public void DoUnlock()
    {
        SetStatus(MapManager.eDistrictStatus.unlocked);

        MapManager.s_Instance.NewZengShouCounter( m_BoundPlanet.GetId(), GetId() );

        DataManager.s_Instance.SaveMyData("TrackUnlock" + m_BoundPlanet.GetId() + "_" + GetId(), 1);

    }



    public int GetPrestigeTimes()
    {
        return m_nPrestigeTimes;
    }

    public void SetPrestigeTimes(int nValue)
    {
        m_nPrestigeTimes = nValue;
    }

    public void DoPrestige()
    {
       
        SetPrestigeTimes(GetPrestigeTimes() + 1);
        Main.s_Instance.ClearAll();
        MapManager.s_Instance.GetCurDistrict().ClearAllRunningPlane();

        UIMsgBox.s_Instance.ShowMsg( "重生成功");

        MapManager.s_Instance.SetZengShouCounterPrestigeTimes( m_BoundPlanet.GetId(), GetId(), GetPrestigeTimes());

        UpdateTrackLevel(1, true); // 重生之后，本赛道的等级又重置为1级

        // 正在加速的(如有)全部停止
        Main.s_Instance.StopAccelerateAll();

        // 本赛道当前所有主管清空
        ClearAllAdmins();

        // 本赛道当前能源系统中的解锁情况全部重置
        // to do

        if ( this == MapManager.s_Instance.GetCurDistrict() )
        {
            //Main.s_Instance._txtPrestigeTimes.text = GetPrestigeTimes().ToString();
            Main.s_Instance.SetPrestigeTimes(GetPrestigeTimes());
        }
    }

    public int GetVehicleBuyTimes( int nLevel )
    {
        int nIndex = nLevel - 1;
        return m_aryVehicleBuyTimes[nIndex];
    }

    public void AddBuyTimes( int nLevel )
    {
        int nIndex = nLevel - 1;
        m_aryVehicleBuyTimes[nIndex]++;
    }

    public void SetVehicleBuyTimeById( int nId, int nTimes )
    {
        m_dicVehicleBuyTimes[nId] = nTimes;
    }


    public int GetVehicleBuyTimeById( int nId )
    {
        int nTimes = 0;

        if ( !m_dicVehicleBuyTimes.TryGetValue( nId, out nTimes) )
        {
            nTimes = 0;
        }

        return nTimes;
    }

    public void GainAdsRaise()
    {
        /*
        float fAdsTimeRaise = ScienceTree.s_Instance.GetAdsTimeRaise();

        m_nAdsRaiseTime += (int)( m_nAdsBaseTime * ( 1 + fAdsTimeRaise) );
        */
        m_nAdsRaiseTime += (int)AdsManager.s_Instance.GetAdsDuration();

        if (MapManager.s_Instance.GetCurDistrict() == this)
        {
            Main.s_Instance.UpdateRaise();
        }
    }

    public void AdsRaiseTimeLoop()
    {
        if (m_nAdsRaiseTime <= 0)
        {
            return;
        }

        m_nAdsRaiseTime -= 1;

        if (m_nAdsRaiseTime <= 0)
        {
            if (MapManager.s_Instance.GetCurDistrict() == this)
            {
                Main.s_Instance.UpdateRaise();
            }
        }
    }

    public int GetAdsLeftTime()
    {
        return m_nAdsRaiseTime;
    }

    public float GetAdsRaise( ref string szAdsPromoteInfo)
    {
        if (m_nAdsRaiseTime <= 0)
        {
            return 0;
        }
        else
        {
            return AdsManager.s_Instance.GetAdsRaise( ref szAdsPromoteInfo);
        }
    }

    ///////////////////// 离线收益相关 /////////////////////

    System.DateTime m_fStartOfflineTime;
    public void SetStartOfflineTime(System.DateTime fTime)
    {
        m_fStartOfflineTime = fTime;
    }

    public System.DateTime GetStartOfflineTime()
    {
        return m_fStartOfflineTime;
    }

    //  离线。注意是指这个District离线，未必是整个app离线了。
    double m_fOfflineGainPerSecondBase = 0f;
    double m_fOfflineGainPerSecondReal = 0f;
    bool m_bOffline = true;
    public void OffLine()
    {
        SetStartOfflineTime( Main.GetSystemTime() );

        m_bOffline = true;
        m_fOfflineGainPerSecondBase = 0;

        /*
        // 离线的时候计算一下当前的离线产出率：暂定为当前在线产出率的10%. 
        List<Plane> lstRunnig = Main.s_Instance.GetRunningList();
        for (int i = 0; i < lstRunnig.Count; i++ )
        {
            Plane plane = lstRunnig[i];
            int nGainCointPerRound = plane.GetCoinGainPerRound(); // 每一圈的收益
            float fTimeOfOneRound = Main.s_Instance.GetOneRoundTimeByLevel(plane.GetLevel()); // 每圈需要多少秒（原始值）
            float fRoundPerSecond = 1f / fTimeOfOneRound; // 每秒多少圈
            // 结合当前的技能树加成，得出每圈实际需要多少秒。这里只考虑科技树加成，不考虑主动技能加成，因为离线状态主动技能无效。
            float fScienceSpeedRaise = ScienceTree.s_Instance.GetAccelerateRaise( m_BoundPlanet.GetId() );
            fRoundPerSecond *= ( 1f + fScienceSpeedRaise);

            float fOffLineGainOfthisPlane = fRoundPerSecond * nGainCointPerRound;
            m_fOfflineGainPerSecondBase += fOffLineGainOfthisPlane;
        } // end for

        m_fOfflineGainPerSecondReal = m_fOfflineGainPerSecondBase * Main.s_Instance.UpdateRaise(m_BoundPlanet, this, true);
        */

        /*
        CalculateDPS();
        m_fOfflineGainPerSecondReal = m_fDPS * 0.1f; // 暂定离线收益是在线收益的十分之一
        */

        CalculateOfflineDps();
    }

    public void CalculateOfflineDps()
    {
        CalculateDPS();
        m_fOfflineGainPerSecondReal = m_fDPS * 0.1f; // 暂定离线收益是在线收益的十分之一
    }

    public double GetOfflineDps()
    {
        return m_fOfflineGainPerSecondReal;
    }

    double m_fDPS = 0f;
    float m_fSpeedAccelerateRate = 0f;
    public float GetSpeedAccelerateRate()
    {
        return m_fSpeedAccelerateRate;
    }

    public double CalculateDPS()
    {
        m_fDPS = 0f;

        List<Plane> lstRunnig = m_lstRunningPlanes; //Main.s_Instance.GetRunningList();
        for (int i = 0; i < lstRunnig.Count; i++)
        {
            Plane plane = lstRunnig[i];
            double nGainCointPerRound = plane.GetCoinGainPerRound(); // 每一圈的收益(原始值)
            float fTimeOfOneRound = Main.s_Instance.GetOneRoundTimeByLevel(plane.GetLevel()); // 每圈需要多少秒（原始值）
            float fRoundPerSecond = 1f / fTimeOfOneRound; // 每秒多少圈


            //////// 速度总加成
            m_fSpeedAccelerateRate = 0;

            // 结合当前的技能树加成，得出每圈实际需要多少秒。
            float fScienceSpeedRaise = ScienceTree.s_Instance.GetAccelerateRaise(m_BoundPlanet.GetId());
           
            if (fScienceSpeedRaise > 0)
            {
                fRoundPerSecond *= (1f + fScienceSpeedRaise);
                m_fSpeedAccelerateRate += (1f + fScienceSpeedRaise);
            }

            // 主动技能造成的速度加成
            float fInitiativeAccelerate = plane.GetInitiativeAccelerateRate();
            if (fInitiativeAccelerate > 1)
            {
                fRoundPerSecond *= fInitiativeAccelerate;
                m_fSpeedAccelerateRate += fInitiativeAccelerate;

            }
            //////// end 速度总加成

            double fDpsOfthisPlane = fRoundPerSecond * nGainCointPerRound;



            m_fDPS += fDpsOfthisPlane;

           

        } // end for
        m_fDPS *= Main.s_Instance.UpdateRaise(m_BoundPlanet, this, true);


        return m_fDPS;
    }


    public double GetCurTotalOfflineGain()
    {
        return m_fCurTotalOfflineGain;

    }

    public void SetCurTotalOfflineGain( float fValue )
    {
         m_fCurTotalOfflineGain = fValue;

        SetStartOfflineTime(Main.GetSystemTime()); // 收取离线收益后，离线时间要刷新

    }

    //  手机和电脑不一样。手机程序失去焦点之后逻辑是完全不运行的
    public float CalculateOffLineProfit()
    {
        System.TimeSpan fTimeElapse = Main.GetSystemTime() -  GetStartOfflineTime();


    
        float fProfit = (float)( m_fOfflineGainPerSecondReal * fTimeElapse.TotalSeconds );
    
        return fProfit;
    }

    public void OnLine()
    {
        m_bOffline = false;


        Main.s_Instance.m_fOfflineProfit = CalculateOffLineProfit();
        if (Main.s_Instance.m_fOfflineProfit > 0)
        {
            Main.s_Instance._panelCollectOfflineProfit.SetActive(true);
            Main.s_Instance._txtOfflineProfitOfThisDistrict.text = "+" + Main.s_Instance.m_fOfflineProfit.ToString("f0");


            OfflineManager.s_Instance.UpdateOfflineProfitInfo();
        }

    }

    // 10秒计算一次收益
    const float OFFLINE_GAIN_INTERVAL = 10f;
    float m_fTimeElapseOffline = 0f;
    double m_fCurTotalOfflineGain = 0f;
    void OfflineLoop()
    {

        // 离线总收入不清零，要领取了才清零

        if ( !m_bOffline)
        {
            return;
        }

        m_fTimeElapseOffline += Time.deltaTime;
        if (m_fTimeElapseOffline < OFFLINE_GAIN_INTERVAL)
        {
            return;
        }
        m_fTimeElapseOffline = 0; 

        double fOfflineGain = m_fOfflineGainPerSecondReal * OFFLINE_GAIN_INTERVAL;
        if (true/*fOfflineGain > 10f*/)
        {
            //Debug.Log( "district[" + GetId() + "]离线收入增加：" + fOfflineGain);
            m_fCurTotalOfflineGain += fOfflineGain;
        }
    }

    /// end 离线收益相关

    public void AddRunningPlane( Plane plane )
    {
        m_lstRunningPlanes.Add(plane);
    }

    public void RemoveRunningPlane(Plane plane)
    {
        m_lstRunningPlanes.Remove(plane);
    }

    public void ClearAllRunningPlane()
    {
        m_lstRunningPlanes.Clear();
    }


    public void SetLevel( int nLevel )
    {
        m_nLevel = nLevel;
    }

    public int GetLevel()
    {
        return m_nLevel;
    }

    // 赛道等级改变
    public void UpdateTrackLevel( int nLevel, bool bDirectly = false )
    {
        if (!bDirectly)
        {
            if (GetLevel() >= nLevel) // 不要重复执行
            {
                return;
            }
        }

        SetLevel(nLevel);
        MapManager.s_Instance.UpdateTrackInfo();

        // 检测是否已达到“能源研究系统”中指定的瓶颈等级
        bool bFound = false;
        ResearchManager.sResearchConfig config = ResearchManager.s_Instance.GetResearchConfig( GetBoundPlanet().GetId(), GetId(), nLevel, ref bFound);
        if (bFound)
        {
            if (m_ResearchCounter == null)
            {
                m_ResearchCounter = ResearchManager.s_Instance.NewResearchCounter();
            }
            m_ResearchCounter.SetBoundTrack( this );
            m_ResearchCounter.Activate(nLevel, config);
        }

        // 更新“掉落”相关的数值
        CalculateDropInfo();


    }

    public void UpdateResearchRealData()
    {
        if (m_ResearchCounter == null)
        {
            return;
        }

        m_ResearchCounter.UpdateRealData();
    }

    public void CalculateAllPlanesSpeed()
    {
        for (int i = 0; i < m_lstRunningPlanes.Count; i++ )
        {
            Plane plane = m_lstRunningPlanes[i];
            plane.CalculateRunningSpeed();
        }
    }

    public bool IsCurTrack()
    {
        return this == MapManager.s_Instance.GetCurDistrict();
    }

    //// 主管相关
    // 添加一个主管
    public Admin AddAdmin( int nId )
    {
        Admin admin = AdministratorManager.s_Instance.NewAdmin();
        admin.SetConfig(AdministratorManager.s_Instance.GetAdminConfig( nId ));
        m_lstAdmins.Add( admin );
        admin.SetBoundTrack( this );

        return admin;
    }

    public void RemoveAdmin( Admin admin )
    {
        m_lstAdmins.Remove( admin );
    }


 

    // 清空所有主管
    public void ClearAllAdmins()
    {
        for (int i = m_lstAdmins.Count - 1; i >= 0; i-- )
        {
            Admin admin = m_lstAdmins[i];
            AdministratorManager.s_Instance.DeleteAdmin(admin);
        }
        m_lstAdmins.Clear();

        AdministratorManager.s_Instance.UpdateAdminMainPanelInfo();

    }

    // 获取本赛道当前已经招募的主管数。相同id的主管可以重复。重复要占用总个数
    public int GetCurBoughtAdminNum()
    {
        return m_lstAdmins.Count;
    }




    public void SetCurUsingAdmin( UIAdministratorCounter admin )
    {
        m_CurUsingAdmin = admin;
    }

    public List<Admin> GetAdminList()
    {
        return m_lstAdmins;
    }

    /// end 主管相关 

    public ResearchCounter GetResearchCounter()
    {
        return m_ResearchCounter;
    }

    // 获取掉落的时间间隔
    public int GetDropInterval()
    {
        if (m_nDropInterval <= 0)
        {
            CalculateDropInfo();

        }

        return m_nDropInterval;
    }

    // 获取可以掉落的载具等级
    public int GetDropLevel()
    {
        if ( m_nDropLevel <= 0 )
        {
            CalculateDropInfo();

        }


        return m_nDropLevel;
    }

    public void CalculateDropInfo()
    {
        string szKey = GetBoundPlanet().GetId() + "_" + GetId() + "_" + GetLevel();
        DataManager.sAutomobileConfig auto_config = DataManager.s_Instance.GetAutomobileConfig(GetBoundPlanet().GetId(), GetId(), GetLevel());
        m_nDropLevel = auto_config.nDropLevel;
        m_nDropInterval = auto_config.nDropInterval;

    }


} // end class
