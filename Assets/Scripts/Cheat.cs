﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cheat : MonoBehaviour {

    public static Cheat s_Instance = null;

    /// <summary>
    /// UI
    /// </summary>
    public Dropdown _dropPlanetId;
    public Dropdown _dropDistrictId;
    public InputField _inputCoin0;
    public InputField _inputCoin1;
    public InputField _inputCoin2;
    public InputField _inputGreenCash;
    public InputField _inputSkillPoint0;
    public InputField _inputSkillPoint1;
    public InputField _inputSkillPoint2;


    public GameObject uiCheatPanel;

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
        Init();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Init()
    {
        List<string> showNames = new List<string>();
        showNames.Add("0 - 青铜");
        showNames.Add("1 - 白银");
        showNames.Add("2 - 黄金");
        UIManager.UpdateDropdownView(_dropPlanetId, showNames);

        showNames.Clear();
        showNames.Add("0赛道");
        showNames.Add("1赛道");
        showNames.Add("2赛道");
        showNames.Add("3赛道");
        showNames.Add("4赛道");
        UIManager.UpdateDropdownView(_dropDistrictId, showNames);
    }

    public void OnClick_Close()
    {
        uiCheatPanel.SetActive( false );
    }

    public void OnClick_Open()
    {
        uiCheatPanel.SetActive( true );
    }

    public void OnClick_UnlockPlanet()
    {
        int nPlanetId = _dropPlanetId.value;
        int nDistrictId = _dropDistrictId.value;
        MapManager.s_Instance.UnLockPlanet(nPlanetId);
    }

    public void OnClick_UnlockDistrict()
    {
        int nPlanetId = _dropPlanetId.value;
        int nDistrictId = _dropDistrictId.value;
        MapManager.s_Instance.UnLockDistrict(nPlanetId, nDistrictId);
    }

    public void OnClick_EnterDistrict()
    {
        int nPlanetId = _dropPlanetId.value;
        int nDistrictId = _dropDistrictId.value;

        MapManager.s_Instance.EnterDistrict(nPlanetId, nDistrictId);
    }

    public void OnClick_ModifyCoin_0()
    {
        double nValue = 0;
        if ( !double.TryParse( _inputCoin0.text, out nValue ) )
        {
            nValue = 0;
        }
        /*
        Planet planet = MapManager.s_Instance.GetPlanetById(0);
        planet.SetCoin( nValue );
        */
        AccountSystem.s_Instance.SetCoin( 0, nValue);
    }

    public void OnClick_ModifyCoin_1()
    {
        double nValue = 0;
        if (!double.TryParse(_inputCoin1.text, out nValue))
        {
            nValue = 0;
        }
        /*
        Planet planet = MapManager.s_Instance.GetPlanetById(1);
        planet.SetCoin(nValue);
        */
        AccountSystem.s_Instance.SetCoin(1, nValue);
    }

    public void OnClick_ModifyCoin_2()
    {
        double nValue = 0;
        if (!double.TryParse(_inputCoin2.text, out nValue))
        {
            nValue = 0;
        }
        /*
        Planet planet = MapManager.s_Instance.GetPlanetById(2);
        planet.SetCoin(nValue);
        */
        AccountSystem.s_Instance.SetCoin(2, nValue);
    }

    public void OnClick_ModifyGreenCash()
    {
        double nValue = 0;
        if (!double.TryParse(_inputGreenCash.text, out nValue))
        {
            nValue = 0;
        }

        AccountSystem.s_Instance.SetGreenCash(nValue);
    }

    public void OnClick_SetSkillPoint0()
    {
        int nValue = 0;
        if (!int.TryParse(_inputSkillPoint0.text, out nValue))
        {
            nValue = 0;
        }
        ScienceTree.s_Instance.SetSkillPoint(ScienceTree.eBranchType.branch0, nValue);
    }

    public void OnClick_SetSkillPoint1()
    {
        int nValue = 0;
        if (!int.TryParse(_inputSkillPoint1.text, out nValue))
        {
            nValue = 0;
        }
        ScienceTree.s_Instance.SetSkillPoint(ScienceTree.eBranchType.branch1, nValue);
    }

    public void OnClick_SetSkillPoint2()
    {
        int nValue = 0;
        if (!int.TryParse(_inputSkillPoint2.text, out nValue))
        {
            nValue = 0;
        }
        ScienceTree.s_Instance.SetSkillPoint(ScienceTree.eBranchType.branch2, nValue);
    }

    public void OnClick_ClearMyData()
    {
        DataManager.s_Instance.ClearMyData();
    }

}
