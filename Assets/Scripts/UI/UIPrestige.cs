﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPrestige : MonoBehaviour {

    public Text _txtCurGainTimes;
    public Text _txtNextGainTimes;
    public Text _txtCost;
    public Button _btnDoPrestige;


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}



    public void UpdateInfo( int nPlanetId = -1, int nDistrictId = -1 )
    {
        Planet planet = null;
        District district = null;
        if (nPlanetId == -1 || nDistrictId == -1)
        {
            nPlanetId = MapManager.s_Instance.GetCurPlanet().GetId();
            nDistrictId = MapManager.s_Instance.GetCurDistrict().GetId();
        }
   
        planet = MapManager.s_Instance.GetPlanetById(nPlanetId);
        district = planet.GetDistrictById(nDistrictId);


        MapManager.s_Instance.m_PlanetToPrestige = planet;
        MapManager.s_Instance.m_DistrictToPrestige = district;

        /*
        int nCurPrestigeTimes = district.GetPrestigeTimes();
        int nCurGainTimes = DataManager.s_Instance.GetPrestigeGain(nPlanetId, nDistrictId, nCurPrestigeTimes);
        int nNextGainTimes = DataManager.s_Instance.GetPrestigeGain(nPlanetId, nDistrictId, nCurPrestigeTimes + 1);
        int nCost = DataManager.s_Instance.GetPrestigaeCoinCost(nPlanetId, nDistrictId);

        _txtCost.text = nCost.ToString();
        _txtCurGainTimes.text = nCurGainTimes.ToString() + "X";
        _txtNextGainTimes.text = nNextGainTimes.ToString() + "X";
        */
        int nCurPrestigeTimes = district.GetPrestigeTimes();
        string szKey = nPlanetId + "_" + nDistrictId;
        DataManager.sPrestigeConfig config = DataManager.s_Instance.GetPrestigeConfig( szKey );


        double dPrestigeCoinCost = 0d;
        if (nCurPrestigeTimes >= config.aryCoinCost.Count || nCurPrestigeTimes >= config.aryCoinPromote.Count) // can not prestige
        {

            _txtCost.text = "";
            _btnDoPrestige.gameObject.SetActive(false);

            float fPromote = config.aryCoinPromote[nCurPrestigeTimes - 1];
            _txtCurGainTimes.text = "提升" + (fPromote * 100f).ToString("f0") + "%";
            _txtNextGainTimes.text = "已满级";
        }
        else // can prestige
        {
            _btnDoPrestige.gameObject.SetActive(true);

            // 这是配置的原始花费。实际花费还会受天赋系统的影响
            dPrestigeCoinCost = config.aryCoinCost[nCurPrestigeTimes];

            float fPrestigeCostReducePercent = ScienceTree.s_Instance.GetPrestigeCostReducePercent();
            double fRealPrestigeCostreduce = (1f - fPrestigeCostReducePercent) * dPrestigeCoinCost;

            float fCurPromote = 0;
            if (nCurPrestigeTimes > 0)
            {
                fCurPromote =  config.aryCoinPromote[nCurPrestigeTimes - 1];
            }
            float fNextPromote = config.aryCoinPromote[nCurPrestigeTimes];

            _txtCost.text = CyberTreeMath.GetFormatMoney(fRealPrestigeCostreduce);
            if (nCurPrestigeTimes == 0)
            {
                _txtCurGainTimes.text = "提升0%";
                _txtNextGainTimes.text = "提升" + (fNextPromote * 100f).ToString("f0") + "%";
            }
            else
            {
                _txtCurGainTimes.text = "提升" + (fCurPromote * 100f).ToString( "f0" ) + "%";
                _txtNextGainTimes.text = "提升" + (fNextPromote * 100f).ToString("f0") + "%";
            }


        }






    }



} // end class
