﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapManager : MonoBehaviour {

    static Vector3 vecTempPos = new Vector3();
    static Vector3 vecTempScale = new Vector3();

    public static MapManager s_Instance = null;

    public string[] m_aryDistrictName;

    public Sprite m_sprBg_Locked_Left;
    public Sprite m_sprBg_Locked_Right;
    public Sprite m_sprBgUnlocked_Left;
    public Sprite m_sprBgUnlocked_Right;

    ///// 各种金币显示UI
    public MoneyCounter[] m_aryMoneyCounters;
    public Image[] m_aryVariousCoinIcons;

    /// end 各种金币显示UI


    /// <summary>
    ///  UI
    /// </summary>
    public GameObject _containerZengShouCounters;
    public GameObject _panelZengShou;

    public GameObject _panelPanelDetails;
    public Text _txtPanelDetailstitle;
    public UIDistrict[] m_aryUIDistricts;
    public UIPlanet[] m_aryUIPlanets;

    public GameObject _panelUnlockDistrict;
    public Text _txtUnlockDistrictCost;
    public Image _imgUnlockTrackPriceIcon;
      
    public GameObject _panelUnlockPlanet;
    public Text _txtUnlockPlanetCost;
    public Image _imgUnlockPlanetPriceIcon;

    public GameObject _panelEntering;

    public GameObject _containerPlanets;

    public GameObject _containerMainPlanets;
    public GameObject _containerOtherPlanets;

    public GameObject _panelScienceTree;


    public Text _txtCurPlanetAndTrack;
    public Text _txtCurTrackLevel;


    // 离线收益相关
    public GameObject _subpanelBatCollectOffline;
    public Text _txtBatOfflineGain;
    public Text _txtBatOfflineDetail;

    //// end UI


    public GameObject _goStarSky;
    public float m_fStarSkyMoveSpeed = 1f;

    public enum ePlanetId
    {
        cooper,
        silver,
        gold,
        activity,
    };

    public enum ePlanetStatus
    {
        unlocked,        // 已解锁
        can_unlock,      // 未解锁，当前可以解锁
        can_not_unlock,  // 未解锁，当前不能解锁
    };

    public enum eDistrictStatus
    {
        unlocked,        // 已解锁
        can_unlock,      // 未解锁，当前可以解锁
        can_not_unlock,  // 未解锁，当前不能解锁
    };

    public Planet[] m_aryPlanets;

    public Planet m_CurPlanet = null;
    public District m_CurDistrict = null;

    public int m_nCurShowPlanetDetialIdOnUI = 0;

    Dictionary<string, UIZengShouCounter> m_dicZengShouCounters = new Dictionary<string, UIZengShouCounter>();

    public Text[] m_aryCoin0Value;
    public Text[] m_aryCoin1Value;
    public Text[] m_aryCoin2Value;
    public Text[] m_aryDiamondValue;

    private void Awake()
    {
        s_Instance = this;
    }

    private void FixedUpdate()
    {
        StarSkyMoveLoop();
    }

    public void Init()
    {


    }

    public void LoadMyData_CurTrackPlanesData()
    {
        // 采取读档机制
        m_CurPlanet = null;// m_aryPlanets[0];
        m_CurDistrict = null;// m_CurPlanet.GetDistrictById(0);


        // 星球解锁情况
        for (int i = 0; i < DataManager.s_Instance.MAX_PLANET_NUM; i++)
        {
            Planet planet = m_aryPlanets[i];
            double dUnlock = (int)DataManager.s_Instance.GetMyData("PlanetUnlock" + i);
            if (dUnlock == 1)
            {
                planet.DoUnlock();
                Debug.Log("星球已解锁：" + i);
            }

            // 赛道解锁情况
            District[] aryTracks = planet.GetDistrictsList();
            for (int j = 0; j < DataManager.s_Instance.MAX_TRACK_NUM_OF_PLANET; j++)
            {
                District track = aryTracks[j];
                dUnlock = (int)DataManager.s_Instance.GetMyData("TrackUnlock" + i + "_" + j);
                if (dUnlock == 1)
                {
                    track.DoUnlock();
                    Debug.Log("赛道已解锁：" + i + "_" + j);
                }

            } // end j

        } // end for i

        string szData = DataManager.s_Instance.GetMyData_String("CurPlanetAndTrack");
        if ( szData == DataManager.LOAD_MY_DATA_INVALID_STRING )
        {
            // 如果没有存档，则默认进入星球0、赛道0
            EnterDistrict(0, 0);
            return;
        }
        string[] aryParmas = szData.Split( '_' );
        int nPlanetId = int.Parse(aryParmas[0]);
        int nTrackId = int.Parse(aryParmas[1]);

        Debug.Log( "当前所处赛道:" + szData);

        EnterDistrict(nPlanetId, nTrackId);




        UpdateTrackInfo();
    }



    // Use this for initialization
    void Start()
    {

     
    }

    public static bool IsTrackKeyValid(string szKey)
    {
        if (szKey == "")
        {
            return false;
        }
        return true;
    }

    public void NewZengShouCounter( int nPlanetId, int nDistrictId )
    {
        UIZengShouCounter zengshou_counter = ResourceManager.s_Instance.NewZengShouCounter().GetComponent<UIZengShouCounter>();
        zengshou_counter.transform.SetParent(_containerZengShouCounters.transform);
        vecTempScale.x = 1f;
        vecTempScale.y = 1f;
        vecTempScale.z = 1f;
        zengshou_counter.transform.localScale = vecTempScale;
        zengshou_counter.SetPlanetIdAndDistrictId(nPlanetId, nDistrictId);

        m_dicZengShouCounters[nPlanetId + "_" + nDistrictId] = zengshou_counter;

     //   int nRate = ( nPlanetId + 1 ) * (nDistrictId + 1);

        float fRate = DataManager.s_Instance.GetTrackConfigById(nPlanetId, nDistrictId).fEarning;

        zengshou_counter._txtDistrictRaise.text = "x" + fRate/*nRate*/;
    }
	
	// Update is called once per frame
	void Update () {
        PreEnterRaceLoop();

        UpdatePlanetInfoLoop();
    }

    float m_fUpdatePlanetInfoLoopTimeElapse = 0;
    void UpdatePlanetInfoLoop()
    {
        if ( !m_bPlanetDetailPanelOpen )
        {
            return;
        }

        m_fUpdatePlanetInfoLoopTimeElapse += Time.deltaTime;
        if (m_fUpdatePlanetInfoLoopTimeElapse< 1f)
        {
            return;
        }
        m_fUpdatePlanetInfoLoopTimeElapse = 0;


        UpdateUIDistrictsInfo();
    }


   
    public void UpdateTrackInfo()
    {
        if (m_CurDistrict == null || m_CurPlanet == null)
        {
            return;
        }
       
            _txtCurTrackLevel.text = "当前赛道等级：" + m_CurDistrict.GetLevel();
       
   
            _txtCurPlanetAndTrack.text = "星球:" + m_CurPlanet.GetId() + ", 赛道：" + (m_CurDistrict.GetId());


        AdministratorManager.s_Instance.UpdateAdminMainPanelInfo();
    }

    public void UnLockPlanet( int nPlanetId )
    {
        if ( nPlanetId == 0 )
        {
            //UIMsgBox.s_Instance.ShowMsg( "0号星球是起始星球，无需解锁" );
            return;
        }

        Planet planet_to_unlock = MapManager.s_Instance.GetPlanetById(nPlanetId);
        Planet planet_prev = MapManager.s_Instance.GetPlanetById(nPlanetId - 1);

       
        if (planet_prev.GetStatus() != ePlanetStatus.unlocked)
        {
            UIMsgBox.s_Instance.ShowMsg("先解锁上一级星球");
            return;
        }

        if (!planet_prev.CheckIfAllDistrictsUnlocked())
        {
            UIMsgBox.s_Instance.ShowMsg("先解锁上一星球所有赛道");
            return;
        }


        double nCoinCost = planet_to_unlock.GetUnlockCoinCost();
        double nCurTotalCoin = planet_prev.GetCoin();
        if (nCurTotalCoin < nCoinCost)
        {
            UIMsgBox.s_Instance.ShowMsg("上一级星球的金币数量不足:" + CyberTreeMath.GetFormatMoney( nCurTotalCoin ) );
            return;
        }

        // 解锁
        // planet_prev.CostCoin(nCoinCost);
        double nCurCoin = AccountSystem.s_Instance.GetCoin(planet_prev.GetId());
        AccountSystem.s_Instance.SetCoin(planet_prev.GetId(), nCurCoin - nCoinCost);
        planet_to_unlock.DoUnlock();


        UIMsgBox.s_Instance.ShowMsg( "星球解锁成功" );
        _panelUnlockPlanet.SetActive( false );
        UpdateUIPlanetsInfo();
    }

    public void UnLockDistrict(int nPlanetId, int nDistrictId)
    {
        Planet planet = GetPlanetById(nPlanetId);
        if ( planet.GetStatus() != ePlanetStatus.unlocked )
        {
            UIMsgBox.s_Instance.ShowMsg( "这个星球都还没解锁，谈不上赛道解锁" );
            return;
        }

        District district_to_unlock = planet.GetDistrictById(nDistrictId);
        if (nDistrictId == 0 || district_to_unlock.GetStatus() == eDistrictStatus.unlocked)
        {
            UIMsgBox.s_Instance.ShowMsg( "该赛道已解锁，无需再做此操作" );
            return;
        }

        District district_prev = planet.GetDistrictById(nDistrictId - 1);
        if (district_prev.GetStatus() != eDistrictStatus.unlocked)
        {
            UIMsgBox.s_Instance.ShowMsg( "先解锁上一级赛道" );
            return;
        }


        double nPriceToUnlock = district_to_unlock.GetUnlockPrice();
        double nCurTotalCoin = planet.GetCoin();
        if (nCurTotalCoin < nPriceToUnlock)
        {
            UIMsgBox.s_Instance.ShowMsg( "星球金币数量不足");
            return;
        }

        // 正式解锁
        //planet.CostCoin(nPriceToUnlock);
        double nCurCoint = AccountSystem.s_Instance.GetCoin(planet.GetId());
        AccountSystem.s_Instance.SetCoin(planet.GetId(),  nCurCoint - nPriceToUnlock);
        district_to_unlock.DoUnlock();

        UIMsgBox.s_Instance.ShowMsg( "赛道解锁成功" );
        _panelUnlockDistrict.SetActive(false);
        UpdateUIDistrictsInfo();



    }

    public void DoSomethingWhenChangeTrack()
    {
        UnlockNewPlaneManager.s_Instance.Reset();
    }

    public void EnterDistrict(int nPlanetId, int nDistrictId)
    {
      
        Planet planet = MapManager.s_Instance.GetPlanetById(nPlanetId);
        if (planet == null) 
        {
            Debug.LogError("planet == null");
            return;
        }
        if (planet.GetStatus() != MapManager.ePlanetStatus.unlocked)
        {
            Debug.Log("星球" + (nPlanetId) + "尚未解锁");
            return;
        }

        Planet cur_planet = MapManager.s_Instance.GetCurPlanet();
        District cur_district = MapManager.s_Instance.GetCurDistrict();

        District district = planet.GetDistrictById(nDistrictId);
        if (district == null)
        {
            Debug.LogError("district == null");
        }

        if (district == cur_district)
        {
            Debug.Log("已经在这个赛道中了");
            return;

        }

        if (district.GetStatus() != MapManager.eDistrictStatus.unlocked)
        {
            Debug.Log("星球" + (nPlanetId) + "的赛道" + (nDistrictId) + "未解锁");
            return;
        }

        MapManager.s_Instance.SaveCurDistrictData();


        m_CurPlanet = planet;

        // 离开的这个District变成离线状态
        if (m_CurDistrict)
        {
            m_CurDistrict.OffLine();
        }
        if (m_CurDistrict != district)
        {
            DoSomethingWhenChangeTrack();
        }

        m_CurDistrict = district;
        m_CurDistrict.OnLine();

      

        Main.s_Instance.Load(m_CurPlanet, m_CurDistrict);

        Main.s_Instance._moneyCoin.SetValue(m_CurPlanet.GetCoin());
        // Main.s_Instance._txtPrestigeTimes.text = m_CurDistrict.GetPrestigeTimes().ToString();
        Main.s_Instance.SetPrestigeTimes(m_CurDistrict.GetPrestigeTimes());


        DebugInfo.s_Instance.SetPlanetAndDistrict(m_CurPlanet.GetId(), m_CurDistrict.GetId());

        Main.s_Instance.UpdateRaise();

        // poppin to do
        // Main.s_Instance.ShowCoin();

        SkillManager.s_Instance.UpdateSkillButtonsStatus();


        // 根据所处的星球，UI上的金币图标显示本星球独有的金币图标
        UpdateCoinIcon(m_CurPlanet.GetId());


        UpdateTrackInfo(  );

        // 存档：当前所在的星球和赛道
        DataManager.s_Instance.SaveMyData("CurPlanetAndTrack", m_CurPlanet.GetId() + "_" + m_CurDistrict.GetId());
    }

    public void UpdateCoinIcon( int nPlanetId )
    {
        Sprite spr = ResourceManager.s_Instance.GetCoinSpriteByPlanetId(nPlanetId);

        for (int i = 0; i < m_aryMoneyCounters.Length; i++ )
        {
            MoneyCounter counter = m_aryMoneyCounters[i];
            if (counter == null)
            {
                continue;
            }
            counter.SetIcon(spr);
        }

        for (int i = 0; i < m_aryVariousCoinIcons.Length; i++ )
        {
            Image imgIcon = m_aryVariousCoinIcons[i];
            if (imgIcon == null)
            {
                continue;
            }
            imgIcon.sprite = spr;
        }


    }



    public Planet GetPlanetById(int nId)
    {
        if (nId < 0 || nId >= m_aryPlanets.Length)
        {
            return null;
        }

        return m_aryPlanets[nId];

    }

    public Planet GetCurPlanet()
    {
        return m_CurPlanet;
    }

    public District GetCurDistrict()
    {
        return m_CurDistrict;
    }

    public void SaveCurDistrictData()
    {
        if (m_CurDistrict == null)
        {
            return;
        }

        string szDistrictData = Main.s_Instance.GenerateData();
        m_CurDistrict.SetData(szDistrictData);


    }

    public Planet m_PlanetToPrestige = null;
    public District m_DistrictToPrestige = null;

    public void OnClick_Prestige()
    {
        /*
        int nCurTotalCoin = m_CurPlanet.GetCoin();
        int nCost = DataManager.s_Instance.GetPrestigaeCoinCost(m_CurPlanet.GetId(), m_CurDistrict.GetId());
        if (nCurTotalCoin < nCost)
        {
            UIMsgBox.s_Instance.ShowMsg("金币不足");
            return;
        }

        m_CurPlanet.CostCoin(nCost);
        m_CurDistrict.DoPrestige(); // 注意，“重生”是针对赛道的，不是针对星球的
        */
        DoPrestige(m_PlanetToPrestige, m_DistrictToPrestige);

        Main.s_Instance._panelPrestige.SetActive( false );

        Main.s_Instance.UpdateRaise();
    }

    public void DoPrestige( Planet planet, District district )
    {
        double nCurTotalCoin = planet.GetCoin();
        int nCost = DataManager.s_Instance.GetPrestigaeCoinCost(planet.GetId(), district.GetId());
        if (nCurTotalCoin < nCost)
        {
            UIMsgBox.s_Instance.ShowMsg("金币不足");
            return;
        }

        //planet.CostCoin(nCost);
        double nCurCoin = AccountSystem.s_Instance.GetCoin(planet.GetId());
        AccountSystem.s_Instance.SetCoin(planet.GetId(), nCurCoin - nCost);
        district.DoPrestige(); // 注意，“重生”是针对赛道的，不是针对星球的
    }

    bool m_bPlanetDetailPanelOpen = false;
    public void OpenPlanetDetailPanel( int nPlanetId )
    {
        m_bPlanetDetailPanelOpen = true;

        m_nCurShowPlanetDetialIdOnUI = nPlanetId;

        _panelPanelDetails.SetActive( true );
        Planet cur_planet = GetPlanetById( nPlanetId );
        _txtPanelDetailstitle.text = GetPlanetNameById(nPlanetId);

        UpdateUIDistrictsInfo();
    }

    public void UpdateUIPlanetsInfo()
    {
        for (int i = 0; i <  m_aryPlanets.Length; i++ )
        {
            UIPlanet ui_planet = m_aryUIPlanets[i];
            Planet planet = m_aryPlanets[i];
            if ( /*i > 0*/true)
            {
                ui_planet.SetUnlockPrice( planet.GetUnlockCoinCost() );
                ui_planet._txtPlanetName.text = DataManager.s_Instance.GetPlanetConfigById( planet.GetId() ).szName;
                if ( planet.GetStatus() == ePlanetStatus.unlocked )
                {
                    ui_planet.SetUnlock( true );
                }
                else
                {
                    ui_planet.SetUnlock(false);
                }
            } // end i > 0
        } // end for
    }

    public void UpdateUIDistrictsInfo()
    {
        Planet cur_show_detail_planet = GetPlanetById(m_nCurShowPlanetDetialIdOnUI);
        District[] aryDistrict = cur_show_detail_planet.GetDistrictsList();
        for (int i = 0; i < aryDistrict.Length; i++)
        {
            District district = aryDistrict[i];
            UIDistrict ui_district = m_aryUIDistricts[i];
            ui_district.SetDistrict(district);


            if (i > 0) // 从第二个赛道开始
            {
                District district_prev = aryDistrict[i - 1];
                bool bPrevDistrictUnlock = (district_prev.GetStatus() == eDistrictStatus.unlocked);
                bool bThisDistrictUnlock = (district.GetStatus() == eDistrictStatus.unlocked);

                if (!bThisDistrictUnlock)
                {
                    ui_district.SetLocked(true, i);
                    ui_district.SetCanUnlock(bPrevDistrictUnlock);
                }
                else
                {
                    ui_district.SetLocked(false, i);
                    ui_district._txtUnlockPrice.gameObject.SetActive(false);
                    //    ui_district._goWenHao.SetActive(false);
                }
                ui_district.m_bLocked = !bThisDistrictUnlock;

            }

            bool bIsCurDistrict = (i == GetCurDistrict().GetId() );
            ui_district._txtIsCurDistrict.gameObject.SetActive(bIsCurDistrict);
            ui_district._contaainerShouYi.gameObject.SetActive(!bIsCurDistrict);
            if (!bIsCurDistrict)
            {
                float fTimeElaspe =(float)( Main.GetSystemTime() - district.GetStartOfflineTime() ).TotalSeconds;
                double fCurOfflineGain = fTimeElaspe * district.GetOfflineDps();
                ui_district._txtShouYi.text = fCurOfflineGain.ToString( "f0" );
            }

            float fAdsLeftTime = district.GetAdsLeftTime();
            if (fAdsLeftTime > 0)
            {
                ui_district._txtTiSheng.text = "提升" + fAdsLeftTime.ToString("f0") + "秒";
            }
            else
            {
                ui_district._txtTiSheng.text = "未提升";
            }

            ui_district._imgMoneyIcon.sprite = ResourceManager.s_Instance.GetCoinSpriteByPlanetId(m_nCurShowPlanetDetialIdOnUI) ;//ResourceManager.s_Instance.m_aryCoinIcon[m_nCurShowPlanetDetialIdOnUI];
            ui_district._imgMoneyIcon_ShouYi.sprite = ResourceManager.s_Instance.GetCoinSpriteByPlanetId(m_nCurShowPlanetDetialIdOnUI);
        } // end for

    }

    public string GetPlanetNameById(int nPlanetId)
    {
        switch(nPlanetId)
        {
            case 0:
                {
                    return "青铜星球";
                }
                break;
            case 1:
                {
                    return "白银星球";
                }
                break;
            case 2:
                {
                    return "黄金星球";
                }
                break;
        } // end switch

        return "";
    }

    public void OnClickButton_ClosePlanetDetailPanel()
    {
        _panelPanelDetails.SetActive( false );
        MapManager.s_Instance._subpanelBatCollectOffline.SetActive(false);
        m_bPlanetDetailPanelOpen = false;
    }

    District m_districtToUnlock = null;
    public void PreUnLockDistrict( District district )
    {
        m_districtToUnlock = district;
        _panelUnlockDistrict.SetActive( true );
        _txtUnlockDistrictCost.text = CyberTreeMath.GetFormatMoney(district.GetUnlockPrice()) ;//district.GetUnlockPrice().ToString();
        _imgUnlockTrackPriceIcon.sprite = ResourceManager.s_Instance.GetCoinSpriteByPlanetId( district.GetBoundPlanet().GetId() );
    }

    public void OnClick_CloseUnlockDistrictPanel()
    {
        _panelUnlockDistrict.SetActive( false );
    }

    public void OnClick_DoUnlockDistrict()
    {
        UnLockDistrict(m_nCurShowPlanetDetialIdOnUI, m_districtToUnlock.GetId());
    }

    Planet m_planetToUnlock = null;
    public void PreUnlockPlanet(int nPlanetId)
    {
        Planet planet_prev = GetPlanetById(nPlanetId - 1);
        if ( planet_prev.GetStatus() != ePlanetStatus.unlocked )
        {
            UIMsgBox.s_Instance.ShowMsg( "上一个星球都还没解锁" );
            return;
        }

        if (!planet_prev.CheckIfAllDistrictsUnlocked())
        {
            UIMsgBox.s_Instance.ShowMsg("先把上一个星球所有赛道解锁");
            return;
        }

        m_planetToUnlock = GetPlanetById(nPlanetId);
        _panelUnlockPlanet.SetActive( true );
        _txtUnlockPlanetCost.text = CyberTreeMath.GetFormatMoney(m_planetToUnlock.GetUnlockCoinCost());//m_planetToUnlock.GetUnlockCoinCost().ToString();

        _imgUnlockPlanetPriceIcon.sprite = ResourceManager.s_Instance.GetCoinSpriteByPlanetId(m_planetToUnlock.GetId() - 1);

    }

    public void OnClick_CloseUnlockPlanetPanel()
    {
        _panelUnlockPlanet.SetActive( false );
    }

    public void OnClick_DoUnlockPlanet()
    {
        UnLockPlanet(m_planetToUnlock.GetId());
    }

    int m_nToEnterRace_PlanetId = 0;
    int m_nToEnterRace_DistrictId = 0;
    public void PreEnterRace( int nPlanetId, int nDistrictId )
    {
        m_nToEnterRace_PlanetId = nPlanetId;
        m_nToEnterRace_DistrictId = nDistrictId;

        m_fPreEnterRaceLoopTime = 1f;

        _panelEntering.SetActive( true );
    }

    float m_fPreEnterRaceLoopTime = 0;
    void PreEnterRaceLoop()
    {
        if (m_fPreEnterRaceLoopTime <= 0)
        {
            return;
        }
        m_fPreEnterRaceLoopTime -= Time.deltaTime;
        if (m_fPreEnterRaceLoopTime <= 0)
        {
            _panelEntering.SetActive(false);
            _panelPanelDetails.SetActive( false );
            Main.s_Instance.OnClickButton_CloseBigMap();
            EnterDistrict(m_nToEnterRace_PlanetId, m_nToEnterRace_DistrictId);
        }
    }

    public Planet[] GetPlanetList()
    {
        return m_aryPlanets;
    }

    bool m_bStarSkyMoving = false;
    public void StartStarSkyEffect()
    {
       // _goStarSky.SetActive( true );
        m_bStarSkyMoving = true;
    }

    public void StopStarSkyEffect()
    {
        _goStarSky.SetActive( false );
        m_bStarSkyMoving = false;

        Camera.main.transform.position = Vector3.zero;;
    }

    // 星空的移动 
    void StarSkyMoveLoop()
    {
        return;

        if (!m_bStarSkyMoving)
        {
            return;
        }

        vecTempPos = Camera.main.transform.position;
        vecTempPos.x -= m_fStarSkyMoveSpeed * Time.fixedDeltaTime;
        Camera.main.transform.position = vecTempPos;


        vecTempPos.x = -Input.gyro.attitude.y * 200f;
        vecTempPos.y = Input.gyro.attitude.x * 200f; ;
        vecTempPos.z = 0;
        _containerMainPlanets.transform.localPosition = vecTempPos;


        vecTempPos.x = -Input.gyro.attitude.y * 50f;
        vecTempPos.y = Input.gyro.attitude.x * 50f;
        vecTempPos.z = 0;
        _containerOtherPlanets.transform.localPosition = vecTempPos;

    }

    public void OnClick_CloseZengShouPanel()
    {
        _panelZengShou.SetActive( false );
    }


    public void OnClick_OpenZengShouPanel()
    {
        _panelZengShou.SetActive(true);

        UpdateZengShouCountersOfflineDps();
    }

    public void SetZengShouCounterPrestigeTimes( int nPlanetId, int nDistrictId, int nPrestigeTimes )
    {
        UIZengShouCounter counter = m_dicZengShouCounters[nPlanetId + "_" + nDistrictId];
        counter._txtCurPrestigeTimes.text = nPrestigeTimes.ToString();

        int nPresitageRaise = DataManager.s_Instance.GetPrestigeGain(nPlanetId, nDistrictId, nPrestigeTimes);
        if (nPresitageRaise == 0)
        {
            nPresitageRaise = 1;
        }
        counter._txtPrestigeRaise.text = "x" + nPresitageRaise;
    }

    public void UpdateZengShouCountersOfflineDps()
    {
        foreach( KeyValuePair<string, UIZengShouCounter> pair in m_dicZengShouCounters )
        {
            UIZengShouCounter counter = pair.Value;
            int nPlanetId = counter.GetPlanetId();
            int nDistrictId = counter.GetDistrictId();
            Planet planet = GetPlanetById(nPlanetId);
            District district = planet.GetDistrictById(nDistrictId);
            district.CalculateOfflineDps();
            counter._txtOfflineDps.text = district.GetOfflineDps().ToString("f0") + "/秒";

        } // end foreach
    }

    public string GetDistrictNameByIndex( int nIndex )
    {
        return m_aryDistrictName[nIndex];
    }

    public void SetAllDiamondValueText( double nValue)
    {
        for (int i = 0; i < m_aryDiamondValue.Length; i++ )
        {
            Text txt = m_aryDiamondValue[i];
            if ( txt == null )
            {
                continue;
            }
            txt.text = CyberTreeMath.GetFormatMoney( nValue ) ;//nValue.ToString("f0");
        }
    }

    public void SetAllCoinValueText( int nPlanetId, double nValue )
    {
        Text[] aryCoinValue = null;
        if (nPlanetId == 0)
        {
            aryCoinValue = m_aryCoin0Value;
        }
        else if (nPlanetId == 1)
        {
            aryCoinValue = m_aryCoin1Value;
        }
        else if (nPlanetId == 2)
        {
            aryCoinValue = m_aryCoin2Value;
        }

        if (aryCoinValue == null)
        {
            return;
        }

        for (int i = 0; i < aryCoinValue.Length; i++ )
        {
            Text txt = aryCoinValue[i];
            if ( txt == null )
            {
                return;
            }

            txt.text = CyberTreeMath.GetFormatMoney(nValue  ) ;// nValue.ToString();
        }

    }

    public void CalculateAllPlanesSpeed()
    {
        m_CurDistrict.CalculateAllPlanesSpeed();
    }

} // end class
