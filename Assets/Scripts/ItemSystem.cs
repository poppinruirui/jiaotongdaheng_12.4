﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemSystem : MonoBehaviour {

    public static Vector3 vecTempScale = new Vector3();

    public static ItemSystem s_Instance = null;

    public GameObject _panelItemBag;

    public GameObject _containerUsing;
    public GameObject _containerNotUsing;

    List<UIItemInBag> m_lstUsingItems = new List<UIItemInBag>();
    List<UIItem> m_lstNowUsingItems = new List<UIItem>();

    public struct sItemConfig
    {
        public int nId; // id
        public string szName; //  名称
        public int nType;     // 道具类型 
        public int nResId;    // 美术资源Id
        public string szParams; // 参数列表 
        public int nDuration;  // 持续时间
        public string szDesc;   // 描述

    };
    sItemConfig tempItemConfig;

    Dictionary<int, sItemConfig> m_dicItemConfig = new Dictionary<int, sItemConfig>();

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {


        string szConfigFileName_Item = DataManager.url + "item.csv";
        StartCoroutine(LoadConfig_Item(szConfigFileName_Item));

    }

    IEnumerator LoadConfig_Item(string szFileName)
    {
        WWW www = new WWW(szFileName);
        yield return www; // 等待下载

        string[] aryLines = www.text.Split('\n');
        for (int i = 1; i < aryLines.Length; i++)
        {
            string[] aryParams = aryLines[i].Split(',');

            int nId = 0;
            if (!int.TryParse(aryParams[0], out nId))
            {
                continue;
            }

            sItemConfig config = new sItemConfig();
            config.nId = nId;
            config.szName = aryParams[1];
            config.nType = int.Parse(aryParams[2]);
            config.nResId = int.Parse(aryParams[3]);
            config.szParams = aryParams[4];
            int.TryParse(aryParams[5], out config.nDuration);
            config.szDesc = aryParams[6];

            m_dicItemConfig[nId] = config;
        } // end for i


        // item.txt文件加载完成之后才开始加载shoppingmall.txt文件，因为后者在解析的过程中要依托前者的配置。
        ShoppinMall.s_Instance.LoadConfig();


    } // end LoadConfig_Item

    public sItemConfig GetItemConfigById( int nItemId )
    {
        if (!m_dicItemConfig.TryGetValue(nItemId, out tempItemConfig))
        {
            Debug.LogError("GetItemConfigById");
        }

        return tempItemConfig;
    }

    // Update is called once per frame
    float m_fTimeElapse = 0f;
	void Update () {
        CountingLoop();
	}

    void CountingLoop()
    {
        // 每秒一次轮询
        m_fTimeElapse += Time.deltaTime;
        if (m_fTimeElapse < 1f)
        {
            return;
        }
        m_fTimeElapse = 0;

        /*
        for (int i = m_lstUsingItems.Count - 1; i >= 0; i-- ) // 如果中途可能删除节点，则应该反向遍历列表
        {
            UIItemInBag item = m_lstUsingItems[i];
            if ( item.DoCount() )
            {
                m_lstUsingItems.Remove( item );
                ResourceManager.s_Instance.DeleteUiItem( item );
                Main.s_Instance.UpdateRaise();
                continue;
            }

        } // end for 
        */

        for (int i = m_lstNowUsingItems.Count - 1; i >= 0; i-- )
        {
            UIItem item = m_lstNowUsingItems[i];
            if ( item.Counting() )
            {
                m_lstNowUsingItems.Remove( item );
                ShoppinMall.s_Instance.DeleteBuyCounter( item );

                DoSomethingWhenItemEnd();

                continue;
            }


        } // end for i

    }

    public void DoSomethingWhenItemEnd()
    {
        Main.s_Instance.UpdateRaise();
        MapManager.s_Instance.CalculateAllPlanesSpeed();
    }

    public void OnClickButton_OpenBag()
    {
        _panelItemBag.SetActive(true);
    }


    public void OnClickButton_CloseBag()
    {
        _panelItemBag.SetActive( false );
    }

    public void AddItem( UIItemInBag item )
    {
        item.transform.SetParent(_containerNotUsing.transform);
        vecTempScale.x = 1.3f;
        vecTempScale.y = 1.3f;
        vecTempScale.z = 1f;
        item.transform.localScale = vecTempScale;
    }

    public UIItem SearchSameIdUsing( UIItem item)
    {
        for (int i = 0; i < m_lstNowUsingItems.Count; i++ )
        {
            if ( item.GetId() == m_lstNowUsingItems[i].GetId())
            {
                return m_lstNowUsingItems[i];
            }
        }

        return null;
    }
    
    public void AddToUsingList( UIItem item )
    {
        m_lstNowUsingItems.Add( item );
        item.SetUseEnalbed( false );
        item.SetPriceVisible( false );
        item.SetLeftTimeVisible( true );
        item.SetNumVisible(false );
      //  item.SetLeftTime();
        item.transform.SetParent( _containerUsing.transform );
        vecTempScale.x = 1f;
        vecTempScale.y = 1f;
        vecTempScale.z = 1f;
        item.transform.localScale = vecTempScale;
    }

    public List<UIItem> GetUsingItemList()
    {
        return m_lstNowUsingItems;
    }

    public void UseItem( UIItem item )
    {
        // 如果是瞬时性道具，则直接体现结果即可。
        // 如果是持续性道具，则去“正在使用”列表中查询，有没有相同Id号的道具正在使用；如有，则直接累加时间，不产生新的使用效果
        bool bNeedNewItem = false;
        switch ((ShoppinMall.eItemType)item.m_BagItemConfig.nType)
        {
            case ShoppinMall.eItemType.coin_raise:
            case ShoppinMall.eItemType.automobile_accelerate:
                {
                    bNeedNewItem = true;
                }
                break;

        } // end switch

        // 生成一个新的Item，用于添加进“正在使用”列表。
        UIItem item_using = null;
        if (bNeedNewItem)
        {
            UIItem exist_item = SearchSameIdUsing(item);
            if (exist_item != null)
            {
                bNeedNewItem = false;

                int nLeftTime = exist_item.GetLeftTime();
                nLeftTime += item.m_BagItemConfig.nDuration;
                exist_item.SetLeftTime(nLeftTime);
            }
            else
            {
                item_using = item.Clone();
            }
           


        }



        switch ( (ShoppinMall.eItemType)item.m_BagItemConfig.nType )
        {
            case ShoppinMall.eItemType.coin_raise:
                {
                    if (bNeedNewItem)
                    {
                        float fRaise = float.Parse(item.m_BagItemConfig.szParams);
                        item_using.SetLeftTime(item.m_BagItemConfig.nDuration);
                        item_using.SetFloatParam(0, fRaise);
                        AddToUsingList(item_using);

                        Main.s_Instance.UpdateRaise();
                    }
                }
                break;
            case ShoppinMall.eItemType.automobile_accelerate:
                {
                    if (bNeedNewItem)
                    {
                        float fRaise = float.Parse(item.m_BagItemConfig.szParams);
                        item_using.SetLeftTime(item.m_BagItemConfig.nDuration);
                        item_using.SetFloatParam(0, fRaise);
                        AddToUsingList(item_using);

                        MapManager.s_Instance.CalculateAllPlanesSpeed();
                    }
                }
                break;
            case ShoppinMall.eItemType.fast_forward:
                {
                    // 获取当前赛道的DPS
                    int nDPS = (int)MapManager.s_Instance.GetCurDistrict().CalculateDPS();
                    int nGainTime = int.Parse(item.m_BagItemConfig.szParams);
                    int nGain = nDPS * nGainTime;
                    string szResult = nDPS + "DPS X" + nGainTime + "秒 =" +  nGain;
                    UIMsgBox.s_Instance.ShowMsg(szResult);

                    int nCurPlanetId = MapManager.s_Instance.GetCurPlanet().GetId();
                    double nCurCoin = AccountSystem.s_Instance.GetCoin(nCurPlanetId);
                    AccountSystem.s_Instance.SetCoin(nCurPlanetId, nCurCoin + nGain);

                }
                break;
            case ShoppinMall.eItemType.treassure_box:
                {
                    TreasureBoxManager.s_Instance.OpenTreasureBox(item);
                }
                break;

        } // end switch


        // 如果当前数量为1，使用之后背包中的道具就消失；如果当前数量大于1，则该道具数量减1，不消失。
        int nCurNum = item.GetNum();
        if (nCurNum > 1)
        {
            item.SetNum(nCurNum - 1);
        }
        else
        {
            ShoppinMall.s_Instance.DeleteBuyCounter(item);
        }

    }

    public void AddItem(UIItem item)
    {
        // 先判断背包中有没有同Id的道具，如果有，就加该道具的数量，而不用重复占一格
        foreach( Transform child in _containerNotUsing.transform )
        {
            UIItem exist_item = child.gameObject.GetComponent<UIItem>();
            if ( exist_item.GetBagItemId() == item.GetBagItemId())
            {
                exist_item.SetNum(exist_item.GetNum() + 1);
                ShoppinMall.s_Instance.DeleteBuyCounter(item);
                return;
            }
        }

        item.SetUseEnalbed( true );
        item.SetLeftTimeVisible( false );
        item.SetNumVisible( true );
        item.SetNum(1);
        item.transform.SetParent(_containerNotUsing.transform);
        vecTempScale.x = 1.3f;
        vecTempScale.y = 1.3f;
        vecTempScale.z = 1f;
        item.transform.localScale = vecTempScale;
    }

    public List<UIItemInBag> GetItemList()
    {
        return m_lstUsingItems;
    }

    public void UseItem(UIItemInBag item)
    {

        for (int i = 0; i < m_lstUsingItems.Count; i++ )
        {
            UIItemInBag item_using = m_lstUsingItems[i];
            if (item_using.m_nItemId == item.m_nItemId)
            {
                item_using.m_nDuration += item.m_nDuration;
                ResourceManager.s_Instance.DeleteUiItem(item);     
               
                return;
            }
        } // end foreach


//        item.SetCountingPanelVisible( true );
//        item.SetUseButtonVisible( false );
        item.m_StartTime = Main.GetSystemTime();
        m_lstUsingItems.Add(item);
        item.transform.SetParent(_containerUsing.transform);



        Main.s_Instance.UpdateRaise();


    }

    public UIItem CreateBagItem( UIItem item_to_buy )
    {
        UIItem item = item_to_buy.Clone();//ShoppinMall.s_Instance.NewBuyCounter();

        item.SetPriceVisible( false );

 

        return item;
    }
     
    public float GetAutomobileSpeedAccelerateByItem()
    {
        float fRaise = 0;

        for (int i = 0; i < m_lstNowUsingItems.Count; i++ )
        {
            UIItem item = m_lstNowUsingItems[i];
            if ( item.m_BagItemConfig.nType != (int)ShoppinMall.eItemType.automobile_accelerate )
            {
                continue;
            }
            fRaise += item.GetFloatParam(0);
        }

        return fRaise;
    }


} // END CLASS
